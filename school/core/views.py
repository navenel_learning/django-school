from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status
from .models import ClassRoom
from .serializers import ClassRoomSerializer


@api_view(['GET', 'DELETE', 'PUT'])
def get_delete_update_classroom(request, pk):

    try:
        classroom = ClassRoom.objects.get(pk=pk)
    except ClassRoom.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    # get details of a single puppy
    if request.method == 'GET':
        serializer = ClassRoomSerializer(classroom)
        return Response(serializer.data)
    # delete a single puppy
    elif request.method == 'DELETE':
        classroom.delete()
        return Response(status=status.HTTP_200_OK)
    # update details of a single puppy
    elif request.method == 'PUT':
        serializer = ClassRoomSerializer(classroom, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_204_NO_CONTENT)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET', 'POST'])
def get_post_classrooms(request):

    # get all puppies
    if request.method == 'GET':
        classrooms = ClassRoom.objects.all()
        serializer = ClassRoomSerializer(classrooms, many=True)
        return Response(serializer.data)
    # insert a new record for a puppy
    elif request.method == 'POST':
        data = {
            'name': request.data.get('name'),
            'level': request.data.get('level')
        }
        serializer = ClassRoomSerializer(data=data)

        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
