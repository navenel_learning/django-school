from django.conf.urls import url
from . import views


urlpatterns = [
    url(
        r'^api/v1/classrooms/(?P<pk>[0-9]+)$',
        views.get_delete_update_classroom,
        name='get_delete_update_classroom'
    ),
    url(
        r'^api/v1/classrooms/$',
        views.get_post_classrooms,
        name='get_post_classrooms'
    )
]
