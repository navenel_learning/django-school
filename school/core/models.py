from django.db import models

# Create your models here.


class ClassRoom(models.Model):
    name = models.CharField(max_length=100)
    created_at = models.DateTimeField(auto_now_add=True)
    level = models.CharField(max_length=10)

    def get_level(self):
        return self.level

    def get_name(self):
        return self.name
