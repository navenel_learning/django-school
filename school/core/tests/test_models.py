from django.test import TestCase
from ..models import ClassRoom


class ClassRoomTest(TestCase):
    """ Test module for ClassRoom model """

    def setUp(self):
        ClassRoom.objects.create(
            name='troisieme albatros', level='3')

        ClassRoom.objects.create(
            name='quatrieme moineau', level='4')

        ClassRoom.objects.create(
            name='seconde pigeon')


    def test_classRoom_type(self):

        classRoom_trois = ClassRoom.objects.get(name='troisieme albatros')
        classRoom_quatre = ClassRoom.objects.get(name='quatrieme moineau')
        classRoom_seconde = ClassRoom.objects.get(name='seconde pigeon')

        self.assertEqual(
            classRoom_trois.get_level(), "3")
        self.assertEqual(
            classRoom_quatre.get_level(), "4")
        self.assertEqual(
            classRoom_seconde.get_name(), "seconde pigeon")
