import json
from rest_framework import status
from django.test import TestCase, Client
from django.urls import reverse
from ..models import ClassRoom
from ..serializers import ClassRoomSerializer


# initialize the APIClient app
client = Client()


class GetAllClassRoomsTest(TestCase):
    """ Test module for GET all classroom API """

    def setUp(self):
        ClassRoom.objects.create(
            name='troisieme albatros', level='3')
        ClassRoom.objects.create(
            name='seconde pigeon', level='2')
        ClassRoom.objects.create(
            name='sixieme poule', level='6')
        ClassRoom.objects.create(
            name='cinquieme canard', level='5')

    def test_get_all_classrooms(self):
        # get API response
        response = client.get(reverse('get_post_classrooms'))
        # get data from db
        classroom = ClassRoom.objects.all()
        serializer = ClassRoomSerializer(classroom, many=True)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

class GetSingleclassRoomTest(TestCase):
    """ Test module for GET single classRoom API """

    def setUp(self):
        self.pigeon = ClassRoom.objects.create(
            name='troisieme pigeon', level='3')
        self.albatros = ClassRoom.objects.create(
            name='seconde albatros', level='2')
        self.poule = ClassRoom.objects.create(
            name='sixieme poule', level='6')


    def test_get_valid_single_classroom(self):

        response = client.get(
            reverse('get_delete_update_classroom', kwargs={'pk': self.pigeon.pk}))
        classroom = ClassRoom.objects.get(pk=self.pigeon.pk)
        serializer = ClassRoomSerializer(classroom)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_invalid_single_classroom(self):
        response = client.get(
            reverse('get_delete_update_classroom', kwargs={'pk': 30}))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

class CreateNewClassRoom(TestCase):
    """ Test module for inserting a new ClassRoom """

    def setUp(self):
        self.valid_payload = {
            'name': 'troisiem pigeon',
            'level': 3
        }
        self.invalid_payload = {
            'name': '',
            'level': 2,
        }

    def test_create_valid_classroom(self):
        response = client.post(
            reverse('get_post_classrooms'),
            data=json.dumps(self.valid_payload),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_create_invalid_classroom(self):
        response = client.post(
            reverse('get_post_classrooms'),
            data=json.dumps(self.invalid_payload),
            content_type='application/json'
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

class DeleteSingleclassRoomTest(TestCase):
    """ Test module for DELETE single classRoom API """

    def setUp(self):
        self.pigeon = ClassRoom.objects.create(
            name='troisieme pigeon', level='3')
        self.albatros = ClassRoom.objects.create(
            name='seconde albatros', level='2')
        self.poule = ClassRoom.objects.create(
            name='sixieme poule', level='6')


    def test_delete_valid_single_classroom(self):

        response = client.delete(
            reverse('get_delete_update_classroom', kwargs={'pk': self.pigeon.pk}))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_delete_unknown_single_classroom(self):
        response = client.delete(
            reverse('get_delete_update_classroom', kwargs={'pk': 30}))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

class UpdateSingleclassRoomTest(TestCase):
    """ Test module for DELETE single classRoom  API """

    def setUp(self):
        # somes information
        self.pigeon = ClassRoom.objects.create(
            name='troisieme pigeon', level='3')
        self.albatros = ClassRoom.objects.create(
            name='seconde albatros', level='2')
        self.poule = ClassRoom.objects.create(
            name='sixieme poule', level='6')
        # test update with valid payload
        self.valid_payload = {
            'name': 'troisieme pigeon',
            'level': 3,
            'id': '1',

        }

        self.invalid_payload = {

            'level': 3,
            'id': '1',

        }
        # test update with unknow payload
        # test update with invalid payload


    def test_update_valid_single_classroom(self):

        response = client.put(
            reverse('get_delete_update_classroom', kwargs={'pk': self.pigeon.pk}),
            data=json.dumps(self.valid_payload),
            content_type='application/json'
        )

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_update_unknown_single_classroom(self):

        response = client.put(
            reverse('get_delete_update_classroom', kwargs={'pk': 30}),
            data=json.dumps(self.valid_payload),
            content_type='application/json'
        )

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_update_invalid_single_classroom(self):

        response = client.put(
            reverse('get_delete_update_classroom', kwargs={'pk': self.pigeon.pk}),
            data=json.dumps(self.invalid_payload),
            content_type='application/json'
        )

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
