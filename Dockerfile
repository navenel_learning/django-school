FROM python:3.8.5-alpine3.12
ENV LANG C.UTF-8

RUN mkdir /django
RUN apk update

ADD requirements.txt /django/requirements.txt
ADD school/ /django/
RUN pip3 install -r /django/requirements.txt
RUN apk  update
EXPOSE 8000

WORKDIR /django/

CMD python3 manage.py runserver 0.0.0.0:8000
#CMD sleep 3600
